package pl.comarch.microcamp.patterns;

import lombok.Getter;

@Getter
public class BucketItem {
    int no;
    String name;

    int value;

    double price;

    public BucketItem(int no, String name, int value, double price) {
        this.no = no;
        this.name = name;
        this.value = value;
        this.price = price;
    }
}
