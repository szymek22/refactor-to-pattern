package pl.comarch.microcamp.patterns.state;

import pl.comarch.microcamp.patterns.Bucket;
import pl.comarch.microcamp.patterns.BucketItem;

public class NewStateBucket extends BucketState {
    public final Bucket bucket;

    public NewStateBucket(Bucket bucket) {
        this.bucket = bucket;
    }

    @Override
   public void addPosition(BucketItem bucketItem) {
      bucket.addPosition(bucketItem);
    }

    @Override
    public BucketState accept() {
        return new AcceptedBucketState(bucket);
    }
}
