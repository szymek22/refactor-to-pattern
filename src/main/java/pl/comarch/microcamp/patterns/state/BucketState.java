package pl.comarch.microcamp.patterns.state;

import pl.comarch.microcamp.patterns.Bucket;
import pl.comarch.microcamp.patterns.BucketItem;
import pl.comarch.microcamp.patterns.strategy.PaymentExecutor;

import java.util.List;

public class BucketState {

    public void pay(String provider, PaymentExecutor paymentExecutor) {
        throw new UnsupportedOperationException("Method pay is not supported in state: " + this.getClass().toString());
    }

    public void addPosition(BucketItem bucketItem) {
        throw new UnsupportedOperationException("Method addPosition is not supported in state: " + this.getClass().toString());
    }

    public BucketState accept() {
        throw new UnsupportedOperationException("Method addPosition is not supported in state: " + this.getClass().toString());
    }


}
