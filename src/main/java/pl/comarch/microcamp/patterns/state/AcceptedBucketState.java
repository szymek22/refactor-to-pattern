package pl.comarch.microcamp.patterns.state;

import pl.comarch.microcamp.patterns.Bucket;
import pl.comarch.microcamp.patterns.strategy.PaymentExecutor;

public class AcceptedBucketState extends BucketState {
    private final Bucket bucket;
    private AcceptedBucketState paymentExecutor;

    public AcceptedBucketState(Bucket bucket) {

        this.bucket = bucket;
    }

    @Override
    public void pay(String provider, PaymentExecutor paymentExecutor) {
        paymentExecutor.pay(bucket.snapshotItems(), provider);
    }
}
