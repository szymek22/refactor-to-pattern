package pl.comarch.microcamp.patterns;

import pl.comarch.microcamp.patterns.state.BucketState;
import pl.comarch.microcamp.patterns.state.NewStateBucket;
import pl.comarch.microcamp.patterns.strategy.BlikPaymentProvider;
import pl.comarch.microcamp.patterns.strategy.P24PaymentProvider;
import pl.comarch.microcamp.patterns.strategy.PaymentExecutor;


public class ShopBucketApplication {

  public static void main(String[] args) {

    PaymentExecutor paymentExecutor = new PaymentExecutor()
            .addProvider("BLIK", new BlikPaymentProvider())
            .addProvider("P24", new P24PaymentProvider());

    Bucket bucket = new Bucket();
    BucketState bucketState = new NewStateBucket(bucket);

    String provider = "BLIK";

    BucketItem e = new BucketItem(1,"Pozycja 1", 1,   0.1d);
    bucketState.addPosition(e);
    BucketItem e2 = new BucketItem(1,"Pozycja 2", 1,  0.2d);
    bucketState.addPosition(e2);

    bucketState = bucketState.accept();

    bucketState.pay(provider, paymentExecutor);

    bucketState.addPosition(e2);

  }

}
