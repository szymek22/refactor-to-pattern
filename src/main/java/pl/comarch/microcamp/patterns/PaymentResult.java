package pl.comarch.microcamp.patterns;

import lombok.Value;

@Value
public class PaymentResult {

    Status status;

    public static PaymentResult success() {
        return new PaymentResult(Status.SUCCESS);
    }

    enum Status {
        SUCCESS,
        FAIl
    }
}
