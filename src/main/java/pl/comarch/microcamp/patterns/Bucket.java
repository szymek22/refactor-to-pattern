package pl.comarch.microcamp.patterns;

import java.util.ArrayList;
import java.util.List;

public class Bucket {

    private final List<BucketItem> items = new ArrayList<>();

    public void addPosition(BucketItem bucketItem) {
        items.add(bucketItem);
    }

    public List<BucketItem> snapshotItems() {
        return List.copyOf(items);
    }
}
