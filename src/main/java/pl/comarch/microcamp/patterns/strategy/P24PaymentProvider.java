package pl.comarch.microcamp.patterns.strategy;

import pl.comarch.microcamp.patterns.BucketItem;
import pl.comarch.microcamp.patterns.PaymentResult;

import java.util.List;
import java.util.stream.Collectors;


public class P24PaymentProvider implements PaymentProvider {
    @Override
    public PaymentResult pay(List<BucketItem> items) {
        System.out.println(
                "Operacja płatności P24 "
                        + items.stream().map(BucketItem::getPrice).collect(Collectors.summingDouble(a -> a)));
        return PaymentResult.success();
    }
}