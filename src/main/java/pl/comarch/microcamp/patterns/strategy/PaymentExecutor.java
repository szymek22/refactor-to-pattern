package pl.comarch.microcamp.patterns.strategy;

import pl.comarch.microcamp.patterns.Bucket;
import pl.comarch.microcamp.patterns.BucketItem;
import pl.comarch.microcamp.patterns.PaymentResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaymentExecutor {
    private Map<String, PaymentProvider> providers = new HashMap<>();

    public PaymentResult pay(List<BucketItem> bucket, String provider) {
        if (providers.containsKey(provider)) {
            return providers.get(provider).pay(bucket);
        }
        throw new IllegalStateException("Unexpected value: " + provider);

      /*  PaymentProvider paymentProvider = switch (provider) {
            case "BLIK" -> new BlikPaymentProvider();
            case "P24" -> new P24PaymentProvider();
            default -> throw new IllegalStateException("Unexpected value: " + provider);
        };
        return paymentProvider.pay(bucket);*/
    }

    public PaymentExecutor addProvider(String key, PaymentProvider provider) {
        providers.put(key, provider);
        return this;

    }
}
