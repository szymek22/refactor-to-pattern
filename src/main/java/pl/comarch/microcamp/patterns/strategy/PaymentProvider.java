package pl.comarch.microcamp.patterns.strategy;

import pl.comarch.microcamp.patterns.BucketItem;
import pl.comarch.microcamp.patterns.PaymentResult;

import java.util.List;

public interface PaymentProvider {
    PaymentResult pay(List<BucketItem> items);
}
